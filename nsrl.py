#!/usr/bin/env python

""" NSRL hash lookup service.  """
from assemblyline.al.common.result import Result, ResultSection, SCORE
from assemblyline.al.service.base import ServiceBase
from assemblyline.common.exceptions import RecoverableError


class NSRL(ServiceBase):
    """ NSRL (Checks a list of known good files using SHA1 and size). """

    SERVICE_CATEGORY = "Filtering"
    SERVICE_DEFAULT_CONFIG = {
        "host": "127.0.0.1",
        "user": "guest",
        "passwd": "guest",
        "port": 5432,
        "db": "nsrl",
        "blacklist": [
            'Disk Wiper',
            'Hacker Tool',
            'Keyboard Logger',
            'Spyware',
            'Steganography',
            'password cracker',
            'password recovery'
        ],
        "puplist": [
            'Computer Investigation',
            'Forensic',
            'Forensic Toolkit',
            'Parental Control',
            'Security',
            'Security tool',
            'activity monitoring',
            'employee monitoring',
            'enterprise network analysis',
            'ids/ips',
            'investigative software',
            'monitoring software',
            'remote access',
            'remote monitoring',
            'video capture'
        ]
    }
    SERVICE_DESCRIPTION = "This service performs hash lookups against the NSRL database of known good files."
    SERVICE_ENABLED = True
    SERVICE_REVISION = ServiceBase.parse_revision('$Id$')
    SERVICE_STAGE = 'FILTER'
    SERVICE_VERSION = '1'
    SERVICE_CPU_CORES = 0.05
    SERVICE_RAM_MB = 64

    def __init__(self, cfg=None):
        super(NSRL, self).__init__(cfg)
        self.blacklist = self.cfg.get('blacklist')
        self.puplist = self.cfg.get('puplist')
        self._params = {
            'host': self.cfg.get('host'),
            'user': self.cfg.get('user'),
            'port': int(self.cfg.get('port')),
            'passwd': self.cfg.get('passwd'),
            'db': self.cfg.get('db'),
            'blacklist': self.blacklist
        }
        self.connection = None

    def start(self):
        self.connection = NSRLDatasource(self.log, **self._params)

    # noinspection PyUnresolvedReferences
    def import_service_deps(self):
        global NSRLDatasource
        from al_services.alsvc_nsrl.datasource.nsrl import NSRL as NSRLDatasource

    def execute(self, request):
        # We have the sha1 digest in the task object so there is no need to
        # fetch the sample for NSRL execution. 
        cur_result = Result()
        try:
            nsrl_version = self.connection.get_version()
        except NSRLDatasource.DatabaseException:
            raise RecoverableError("Version query failed")
        try:
            dbresults = self.connection.query(request.sha1)
        except NSRLDatasource.DatabaseException:
            raise RecoverableError("Query failed")

        # If we found a result in the NSRL database, drop this task as we don't want to process it further.
        if dbresults:
            unqi_dbresults = list(set(dbresults))
            flagged_bl = False
            flagged_pl = False
            found = "This file was found in the NSRL database (version {})." .format(nsrl_version)
            res = ResultSection(title_text=found)
            for dbresult in unqi_dbresults[:10]:
                apptypes = dbresult[6].split(",")
                for app in apptypes:
                    if app in self.blacklist:
                        flagged_bl = True
                    if app in self.puplist:
                        flagged_pl = True
                res.add_line(dbresult[0] + " - %s (%s) - v: %s - by: %s [%s]. Application type(s): %s"
                             % (dbresult[1], dbresult[2], dbresult[3],
                                dbresult[4], dbresult[5], dbresult[6]))

            if len(unqi_dbresults) > 10:
                res.add_line("And %s more..." % str(len(unqi_dbresults) - 10))

            # Check if in flagged lists, let blacklist win over score. Else filter with neg score.
            if flagged_pl and not flagged_bl:
                des = "This file has been flagged as potentially unwanted software."
                respl = ResultSection(title_text=des, score=SCORE.INFO)
                cur_result.add_section(respl)
            elif flagged_bl:
                des = "This file has been flagged as malicious software."
                respl = ResultSection(title_text=des, score=SCORE.VHIGH)
                cur_result.add_section(respl)
            else:
                request.drop()
                res.score = SCORE.NOT
            cur_result.add_section(res)
        request.result = cur_result


if __name__ == '__main__':
    import pprint
    import sys

    nsrl = NSRL()
    if len(sys.argv) != 2:
        print "Usage: %s <SHA1>" % sys.argv[0]
        exit(1)

    SHA1LEN = 40
    value = sys.argv[1].strip().upper()
    if len(value) != SHA1LEN:
        print "Invalid SHA1. Should be %s chars. Actual: %s : %s" % (
            SHA1LEN, len(value), sys.argv[1]
        )
        exit(1)

    result = nsrl.lookup(value)
    if not result:
        print 'Not Found: %s' % value
        exit(0)

    pprint.pprint(result)
