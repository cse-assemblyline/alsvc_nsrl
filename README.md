# NSRL Service **[v3 service version no longer maintained]**

Performs hash lookups against NIST's [National Software Reference Library](http://www.nsrl.nist.gov/) database.

**NOTE**: This service **requires** you to download and install the NSRL database on a seperate server or VM. 
It is **not** preinstalled during a default installation

## Execution

This service works by querying the NSRL database via direct database access to see if the file that you are submitting 
was catalogued by the NIST as part of the National Software Reference Library. 

NSRL is set to run under the Filtering stage of AL execution by default (for more information on service stages, see 
your AL server's configuration page: https://[AL SERVER HOST]/configuration.html). Therefore, if flagged as known 
software, the sample will NOT continue to be submitted to other services. The service will score a sample found in its 
database a value of -1000, unless the software is assigned a blacklist/puplist category (see below for more details).

## Submission Parameters:

- Ignore Filtering Service: A user can select this option to have the sample continue being processed by other services,
regardless of whether the sample is found in NSRL's database.

## Config (set by administrator):

- "host": IP of the database server.
- "user": Username to access the NSRL database.
- "passwd": Password to access the NSRL database.
- "port": Service port of the postgresql database.
- "db": Database name (should be "nsrl" unless changed when running install script).
- "blacklist": Categories of potentially malicious software. If a sample belongs to a category in this list the NSRL
service will score the sample a value of 500, and will send the sample to other services. Default list:

            'Disk Wiper',
            'Hacker Tool',
            'Keyboard Logger',
            'Spyware',
            'Steganography',
            'password cracker',
            'password recovery'
 
- "puplist": Categories of potentially unwanted software. If a sample belongs to a category in this list the NSRL
service will score the sample a value of 0, and will send the sample to other services. Default list:

            'Computer Investigation',
            'Forensic',
            'Forensic Toolkit',
            'Parental Control',
            'Security',
            'Security tool',
            'activity monitoring',
            'employee monitoring',
            'enterprise network analysis',
            'ids/ips',
            'investigative software',
            'monitoring software',
            'remote access',
            'remote monitoring',
            'video capture'

## Datasource

Because this service only uses the hash to query for information, by installing this service you also enable it as a 
datasource in the hash_search API (`/api/v3/hash_search/<HASH>/`)

## Download/Installation of NSRL DB

You can download the NSRL hashset from the NIST website at
 [http://www.nsrl.nist.gov/Downloads.htm](http://www.nsrl.nist.gov/Downloads.htm).

After that, follow the [installation instructions](db/install_notes.md)
