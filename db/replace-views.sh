#!/bin/sh

VERSION=$1
USER=$2

if [ "$#" -ne 2 ]; then
    echo "Please supply <VERSION> AND <USERNAME>"
    exit 4
fi

TBL_COUNT=$(sudo -u postgres psql -dnsrl -c "\dt" | grep ${VERSION} | wc -l)
if [ ${TBL_COUNT} -eq 0 ]; then
    echo "ERR: Version $VERSION doesn't exist in the server. Load with load-nsrl.sh before proceeding..."
    exit 4
fi

EXISTS=$(sudo -u postgres psql -qt -c "SELECT usename FROM pg_user where usename='${USER}';" | tr -d " \t\n\r")
if [ "${USER}" != "${EXISTS}" ] || [ "${USER}" = '' ]; then
    echo "User ${USER} does not exist in database. Exiting."
    exit 4
fi

echo "Replacing views..."
sudo -u postgres psql -qt nsrl >/dev/null 2>&1 <<EOF
SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = 'nsrl' and pid <> pg_backend_pid();
create or replace view file as select * from file_${VERSION};
create or replace view manufacturer as select * from manufacturer_${VERSION};
create or replace view os as select * from os_${VERSION};
create or replace view product as select * from product_${VERSION};
create or replace view nsrl as
select file.name as name,
       product.name as product,
       os.name as os,
       product.version as version,
       manufacturer.name as manufacturer,
       product.language as language,
       file.md5 as md5,
       file.sha1 as sha1,
       file.size as size,
       product.application as application
from file
inner join os on file.os_code = os.code
inner join product on file.product_code = product.code
inner join manufacturer on product.manufacturer_code = manufacturer.code;
grant select on nsrl to ${USER};
EOF

echo "All done!"