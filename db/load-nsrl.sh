#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

if [ $# -lt 2 ]; then
    echo "usage $0 <VERSION> <RDS ISO 1> ... <RDS ISO X>" >&2
    exit 2
fi

VERSION=$1
for iso in "$@"
do
    if [ ${iso} != ${VERSION} ]; then
        ext=$(echo ${iso} | cut -d '.' -f 2 )
        if [ "${ext}" != "iso" ]; then
            echo "ERR: All files must be ISO files!"
            exit 3
        fi
    fi
done

TBL_COUNT=$(sudo -u postgres psql -dnsrl -c "\dt" | grep ${VERSION} | wc -l)
if [ ${TBL_COUNT} -ne 0 ]; then
    echo "ERR: Version $VERSION already exist in the server. Manually remove it before proceeding..."
    exit 4
fi

echo "Creation tables for version $VERSION..."
sudo -u postgres psql -qt nsrl <<EOF
create table file_${VERSION} as select * from file_template where 0 = 1;
create table manufacturer_${VERSION} as
    select * from manufacturer_template where 0 = 1;
create table os_${VERSION} as select * from os_template where 0 = 1;
create table product_${VERSION} as select * from product_template where 0 = 1;
EOF

for iso in "$@"
do
    if [ ${iso} != ${VERSION} ]; then
        Base=/tmp/nsrl_${VERSION}
        echo "Processing RDS iso file: ${iso}..."
        rm -rf ${Base}
        rm -rf ${Base}_extracted

        mkdir ${Base}
        mount -o ro,loop ${iso} ${Base}

        echo "    Extracting hashes..."
        mkdir -p ${Base}_extracted
        cp ${Base}/* ${Base}_extracted
        umount ${Base}

        (cd ${Base}_extracted && unzip NSRLFile.txt.zip && rm NSRLFile.txt.zip)

        Manufacturer=${Base}_extracted/NSRLMfg.txt
        OS=${Base}_extracted/NSRLOS.txt
        Product=${Base}_extracted/NSRLProd.txt
        File=${Base}_extracted/NSRLFile.txt

        echo "    Fixing potential double quotes errors..."
        cat ${Manufacturer} | sed -e "s/\\\\\"//g" > ${Manufacturer}.fixed &&
        rm ${Manufacturer} &&
        mv ${Manufacturer}.fixed ${Manufacturer}

        cat ${OS} | sed -e "s/\\\\\"//g" > ${OS}.fixed &&
        rm ${OS} &&
        mv ${OS}.fixed ${OS}

        cat ${Product} | sed -e "s/\\\\\"//g" > ${Product}.fixed &&
        rm ${Product} &&
        mv ${Product}.fixed ${Product}

        echo "    importing manufacturers..."
(cat <<EOF
copy manufacturer_${VERSION} from stdin csv;
EOF
tail -n +2 ${Manufacturer} | iconv -f LATIN1) |
sudo -u postgres psql -qt nsrl

        echo "    importing OSes..."
(cat <<EOF
copy os_${VERSION} from stdin csv;
EOF
tail -n +2 ${OS} | iconv -f LATIN1) |
sudo -u postgres psql -qt nsrl

        echo "    importing products..."
(cat <<EOF
copy product_${VERSION} from stdin csv;
EOF
tail -n +2 ${Product} | iconv -f LATIN1) |
sudo -u postgres psql -qt nsrl

        echo "    importing files..."
(cat <<EOF
copy file_${VERSION} from stdin csv;
EOF
tail -n +2 ${File} | iconv -f LATIN1) |
sudo -u postgres psql -qt nsrl

        echo "Done with $iso. Cleaning up..."
        rm -rf ${Base}
        rm -rf ${Base}_extracted
    fi
done

echo "Creating indexes..."
sudo -u postgres psql -qt nsrl <<EOF
create index manufacturer_${VERSION}_code on manufacturer_${VERSION}(code);
create index os_${VERSION}_code on os_${VERSION}(code);
create index product_${VERSION}_code on product_${VERSION}(code);
create index file_${VERSION}_md5 on file_${VERSION}(md5);
create index file_${VERSION}_sha1 on file_${VERSION}(sha1);
EOF

echo "All done!"