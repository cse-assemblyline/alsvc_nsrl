# This is the procedure to install the NSRL Database in postgresql

This guide was tested for NSRL 2.61, but should work with any NSRL release iso.

Follow all instructions to install and populate the NSRL database.
If you are simply updating the database with new NSRL version data,
complete only the "Load NSRL DB" and "Create Views" steps.

## Download NSRL

    cd
    wget https://s3.amazonaws.com/rds.nsrl.nist.gov/RDS/current/RDS_modern.iso

    # Optional
    wget https://s3.amazonaws.com/rds.nsrl.nist.gov/RDS/current/RDS_android.iso
    wget https://s3.amazonaws.com/rds.nsrl.nist.gov/RDS/current/RDS_ios.iso
    wget https://s3.amazonaws.com/rds.nsrl.nist.gov/RDS/current/RDS_legacy.iso

## Install PostgreSQL
Note: Do NOT type the user's password for this step.
Type the string 'password' as indicated, and create a username for
'< USER >'.

    cd /opt/al/pkg/al_services/alsvc_nsrl/db
    ./install-postgres.sh

    sudo nano /etc/postgresql/9.3/main/postgresql.conf
    # Change the following
    listen_addresses = '*'
    max_connections = 1024
    shared_buffers = 8GB
    work_mem = 16MB

    sudo nano /etc/postgresql/9.3/main/pg_hba.conf
    local   nsrl    <USER>   password
    host    nsrl    <USER>   <NETWORK CIDR> password

## Create NSRL DB
Note: Create the user's password for '< PASSWORD >' below.

    cd /opt/al/pkg/al_services/alsvc_nsrl/db
    sudo -u postgres ./create-nsrl.sh <USER> <PASSWORD>

## Load NSRL DB

    cd /opt/al/pkg/al_services/alsvc_nsrl/db
    sudo ./load-nsrl.sh <VERSION_NUMBER> ~/RDS_*.iso

## Create Views
    cd /opt/al/pkg/al_services/alsvc_nsrl/db
    sudo ./replace-views.sh <VERSION_NUMBER> <USER>